// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'dropdownitems_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$DropdownitemsEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() getAllDropDownEvent,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? getAllDropDownEvent,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? getAllDropDownEvent,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(GetAllDropDownEvent value) getAllDropDownEvent,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(GetAllDropDownEvent value)? getAllDropDownEvent,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(GetAllDropDownEvent value)? getAllDropDownEvent,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $DropdownitemsEventCopyWith<$Res> {
  factory $DropdownitemsEventCopyWith(
          DropdownitemsEvent value, $Res Function(DropdownitemsEvent) then) =
      _$DropdownitemsEventCopyWithImpl<$Res, DropdownitemsEvent>;
}

/// @nodoc
class _$DropdownitemsEventCopyWithImpl<$Res, $Val extends DropdownitemsEvent>
    implements $DropdownitemsEventCopyWith<$Res> {
  _$DropdownitemsEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$GetAllDropDownEventImplCopyWith<$Res> {
  factory _$$GetAllDropDownEventImplCopyWith(_$GetAllDropDownEventImpl value,
          $Res Function(_$GetAllDropDownEventImpl) then) =
      __$$GetAllDropDownEventImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$GetAllDropDownEventImplCopyWithImpl<$Res>
    extends _$DropdownitemsEventCopyWithImpl<$Res, _$GetAllDropDownEventImpl>
    implements _$$GetAllDropDownEventImplCopyWith<$Res> {
  __$$GetAllDropDownEventImplCopyWithImpl(_$GetAllDropDownEventImpl _value,
      $Res Function(_$GetAllDropDownEventImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$GetAllDropDownEventImpl implements GetAllDropDownEvent {
  const _$GetAllDropDownEventImpl();

  @override
  String toString() {
    return 'DropdownitemsEvent.getAllDropDownEvent()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$GetAllDropDownEventImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() getAllDropDownEvent,
  }) {
    return getAllDropDownEvent();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? getAllDropDownEvent,
  }) {
    return getAllDropDownEvent?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? getAllDropDownEvent,
    required TResult orElse(),
  }) {
    if (getAllDropDownEvent != null) {
      return getAllDropDownEvent();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(GetAllDropDownEvent value) getAllDropDownEvent,
  }) {
    return getAllDropDownEvent(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(GetAllDropDownEvent value)? getAllDropDownEvent,
  }) {
    return getAllDropDownEvent?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(GetAllDropDownEvent value)? getAllDropDownEvent,
    required TResult orElse(),
  }) {
    if (getAllDropDownEvent != null) {
      return getAllDropDownEvent(this);
    }
    return orElse();
  }
}

abstract class GetAllDropDownEvent implements DropdownitemsEvent {
  const factory GetAllDropDownEvent() = _$GetAllDropDownEventImpl;
}

/// @nodoc
mixin _$DropdownitemsState {
  List<RelationShipModel>? get relationshiplist =>
      throw _privateConstructorUsedError;
  List<DocumentTypeModel>? get documentlist =>
      throw _privateConstructorUsedError;
  List<BranchModel>? get branches => throw _privateConstructorUsedError;
  List<SchemeListModel>? get schemeslist => throw _privateConstructorUsedError;
  List<SchemeTenureModel>? get schemetenures =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(
            List<RelationShipModel>? relationshiplist,
            List<DocumentTypeModel>? documentlist,
            List<BranchModel>? branches,
            List<SchemeListModel>? schemeslist,
            List<SchemeTenureModel>? schemetenures)
        getAllDrpDwnData,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(
            List<RelationShipModel>? relationshiplist,
            List<DocumentTypeModel>? documentlist,
            List<BranchModel>? branches,
            List<SchemeListModel>? schemeslist,
            List<SchemeTenureModel>? schemetenures)?
        getAllDrpDwnData,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(
            List<RelationShipModel>? relationshiplist,
            List<DocumentTypeModel>? documentlist,
            List<BranchModel>? branches,
            List<SchemeListModel>? schemeslist,
            List<SchemeTenureModel>? schemetenures)?
        getAllDrpDwnData,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(GetAllDrpDwnDataState value) getAllDrpDwnData,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(GetAllDrpDwnDataState value)? getAllDrpDwnData,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(GetAllDrpDwnDataState value)? getAllDrpDwnData,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $DropdownitemsStateCopyWith<DropdownitemsState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $DropdownitemsStateCopyWith<$Res> {
  factory $DropdownitemsStateCopyWith(
          DropdownitemsState value, $Res Function(DropdownitemsState) then) =
      _$DropdownitemsStateCopyWithImpl<$Res, DropdownitemsState>;
  @useResult
  $Res call(
      {List<RelationShipModel>? relationshiplist,
      List<DocumentTypeModel>? documentlist,
      List<BranchModel>? branches,
      List<SchemeListModel>? schemeslist,
      List<SchemeTenureModel>? schemetenures});
}

/// @nodoc
class _$DropdownitemsStateCopyWithImpl<$Res, $Val extends DropdownitemsState>
    implements $DropdownitemsStateCopyWith<$Res> {
  _$DropdownitemsStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? relationshiplist = freezed,
    Object? documentlist = freezed,
    Object? branches = freezed,
    Object? schemeslist = freezed,
    Object? schemetenures = freezed,
  }) {
    return _then(_value.copyWith(
      relationshiplist: freezed == relationshiplist
          ? _value.relationshiplist
          : relationshiplist // ignore: cast_nullable_to_non_nullable
              as List<RelationShipModel>?,
      documentlist: freezed == documentlist
          ? _value.documentlist
          : documentlist // ignore: cast_nullable_to_non_nullable
              as List<DocumentTypeModel>?,
      branches: freezed == branches
          ? _value.branches
          : branches // ignore: cast_nullable_to_non_nullable
              as List<BranchModel>?,
      schemeslist: freezed == schemeslist
          ? _value.schemeslist
          : schemeslist // ignore: cast_nullable_to_non_nullable
              as List<SchemeListModel>?,
      schemetenures: freezed == schemetenures
          ? _value.schemetenures
          : schemetenures // ignore: cast_nullable_to_non_nullable
              as List<SchemeTenureModel>?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$GetAllDrpDwnDataStateImplCopyWith<$Res>
    implements $DropdownitemsStateCopyWith<$Res> {
  factory _$$GetAllDrpDwnDataStateImplCopyWith(
          _$GetAllDrpDwnDataStateImpl value,
          $Res Function(_$GetAllDrpDwnDataStateImpl) then) =
      __$$GetAllDrpDwnDataStateImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {List<RelationShipModel>? relationshiplist,
      List<DocumentTypeModel>? documentlist,
      List<BranchModel>? branches,
      List<SchemeListModel>? schemeslist,
      List<SchemeTenureModel>? schemetenures});
}

/// @nodoc
class __$$GetAllDrpDwnDataStateImplCopyWithImpl<$Res>
    extends _$DropdownitemsStateCopyWithImpl<$Res, _$GetAllDrpDwnDataStateImpl>
    implements _$$GetAllDrpDwnDataStateImplCopyWith<$Res> {
  __$$GetAllDrpDwnDataStateImplCopyWithImpl(_$GetAllDrpDwnDataStateImpl _value,
      $Res Function(_$GetAllDrpDwnDataStateImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? relationshiplist = freezed,
    Object? documentlist = freezed,
    Object? branches = freezed,
    Object? schemeslist = freezed,
    Object? schemetenures = freezed,
  }) {
    return _then(_$GetAllDrpDwnDataStateImpl(
      relationshiplist: freezed == relationshiplist
          ? _value._relationshiplist
          : relationshiplist // ignore: cast_nullable_to_non_nullable
              as List<RelationShipModel>?,
      documentlist: freezed == documentlist
          ? _value._documentlist
          : documentlist // ignore: cast_nullable_to_non_nullable
              as List<DocumentTypeModel>?,
      branches: freezed == branches
          ? _value._branches
          : branches // ignore: cast_nullable_to_non_nullable
              as List<BranchModel>?,
      schemeslist: freezed == schemeslist
          ? _value._schemeslist
          : schemeslist // ignore: cast_nullable_to_non_nullable
              as List<SchemeListModel>?,
      schemetenures: freezed == schemetenures
          ? _value._schemetenures
          : schemetenures // ignore: cast_nullable_to_non_nullable
              as List<SchemeTenureModel>?,
    ));
  }
}

/// @nodoc

class _$GetAllDrpDwnDataStateImpl implements GetAllDrpDwnDataState {
  const _$GetAllDrpDwnDataStateImpl(
      {required final List<RelationShipModel>? relationshiplist,
      required final List<DocumentTypeModel>? documentlist,
      required final List<BranchModel>? branches,
      required final List<SchemeListModel>? schemeslist,
      required final List<SchemeTenureModel>? schemetenures})
      : _relationshiplist = relationshiplist,
        _documentlist = documentlist,
        _branches = branches,
        _schemeslist = schemeslist,
        _schemetenures = schemetenures;

  final List<RelationShipModel>? _relationshiplist;
  @override
  List<RelationShipModel>? get relationshiplist {
    final value = _relationshiplist;
    if (value == null) return null;
    if (_relationshiplist is EqualUnmodifiableListView)
      return _relationshiplist;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(value);
  }

  final List<DocumentTypeModel>? _documentlist;
  @override
  List<DocumentTypeModel>? get documentlist {
    final value = _documentlist;
    if (value == null) return null;
    if (_documentlist is EqualUnmodifiableListView) return _documentlist;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(value);
  }

  final List<BranchModel>? _branches;
  @override
  List<BranchModel>? get branches {
    final value = _branches;
    if (value == null) return null;
    if (_branches is EqualUnmodifiableListView) return _branches;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(value);
  }

  final List<SchemeListModel>? _schemeslist;
  @override
  List<SchemeListModel>? get schemeslist {
    final value = _schemeslist;
    if (value == null) return null;
    if (_schemeslist is EqualUnmodifiableListView) return _schemeslist;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(value);
  }

  final List<SchemeTenureModel>? _schemetenures;
  @override
  List<SchemeTenureModel>? get schemetenures {
    final value = _schemetenures;
    if (value == null) return null;
    if (_schemetenures is EqualUnmodifiableListView) return _schemetenures;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(value);
  }

  @override
  String toString() {
    return 'DropdownitemsState.getAllDrpDwnData(relationshiplist: $relationshiplist, documentlist: $documentlist, branches: $branches, schemeslist: $schemeslist, schemetenures: $schemetenures)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$GetAllDrpDwnDataStateImpl &&
            const DeepCollectionEquality()
                .equals(other._relationshiplist, _relationshiplist) &&
            const DeepCollectionEquality()
                .equals(other._documentlist, _documentlist) &&
            const DeepCollectionEquality().equals(other._branches, _branches) &&
            const DeepCollectionEquality()
                .equals(other._schemeslist, _schemeslist) &&
            const DeepCollectionEquality()
                .equals(other._schemetenures, _schemetenures));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(_relationshiplist),
      const DeepCollectionEquality().hash(_documentlist),
      const DeepCollectionEquality().hash(_branches),
      const DeepCollectionEquality().hash(_schemeslist),
      const DeepCollectionEquality().hash(_schemetenures));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$GetAllDrpDwnDataStateImplCopyWith<_$GetAllDrpDwnDataStateImpl>
      get copyWith => __$$GetAllDrpDwnDataStateImplCopyWithImpl<
          _$GetAllDrpDwnDataStateImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(
            List<RelationShipModel>? relationshiplist,
            List<DocumentTypeModel>? documentlist,
            List<BranchModel>? branches,
            List<SchemeListModel>? schemeslist,
            List<SchemeTenureModel>? schemetenures)
        getAllDrpDwnData,
  }) {
    return getAllDrpDwnData(
        relationshiplist, documentlist, branches, schemeslist, schemetenures);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(
            List<RelationShipModel>? relationshiplist,
            List<DocumentTypeModel>? documentlist,
            List<BranchModel>? branches,
            List<SchemeListModel>? schemeslist,
            List<SchemeTenureModel>? schemetenures)?
        getAllDrpDwnData,
  }) {
    return getAllDrpDwnData?.call(
        relationshiplist, documentlist, branches, schemeslist, schemetenures);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(
            List<RelationShipModel>? relationshiplist,
            List<DocumentTypeModel>? documentlist,
            List<BranchModel>? branches,
            List<SchemeListModel>? schemeslist,
            List<SchemeTenureModel>? schemetenures)?
        getAllDrpDwnData,
    required TResult orElse(),
  }) {
    if (getAllDrpDwnData != null) {
      return getAllDrpDwnData(
          relationshiplist, documentlist, branches, schemeslist, schemetenures);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(GetAllDrpDwnDataState value) getAllDrpDwnData,
  }) {
    return getAllDrpDwnData(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(GetAllDrpDwnDataState value)? getAllDrpDwnData,
  }) {
    return getAllDrpDwnData?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(GetAllDrpDwnDataState value)? getAllDrpDwnData,
    required TResult orElse(),
  }) {
    if (getAllDrpDwnData != null) {
      return getAllDrpDwnData(this);
    }
    return orElse();
  }
}

abstract class GetAllDrpDwnDataState implements DropdownitemsState {
  const factory GetAllDrpDwnDataState(
          {required final List<RelationShipModel>? relationshiplist,
          required final List<DocumentTypeModel>? documentlist,
          required final List<BranchModel>? branches,
          required final List<SchemeListModel>? schemeslist,
          required final List<SchemeTenureModel>? schemetenures}) =
      _$GetAllDrpDwnDataStateImpl;

  @override
  List<RelationShipModel>? get relationshiplist;
  @override
  List<DocumentTypeModel>? get documentlist;
  @override
  List<BranchModel>? get branches;
  @override
  List<SchemeListModel>? get schemeslist;
  @override
  List<SchemeTenureModel>? get schemetenures;
  @override
  @JsonKey(ignore: true)
  _$$GetAllDrpDwnDataStateImplCopyWith<_$GetAllDrpDwnDataStateImpl>
      get copyWith => throw _privateConstructorUsedError;
}
